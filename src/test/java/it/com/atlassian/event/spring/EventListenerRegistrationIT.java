package it.com.atlassian.event.spring;

import com.atlassian.event.api.EventPublisher;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class EventListenerRegistrationIT {
    @Resource
    private ExampleAnnotationBasedEventListener singletonScopedListener;

    @Resource
    private EventPublisher eventPublisher;

    @After
    public void resetListeners() {
        singletonScopedListener.reset();
    }

    @Test
    public void testThatEventsArePublishedToListener() {
        ExampleEvent event = new ExampleEvent();
        eventPublisher.publish(event);
        assertThat(singletonScopedListener.event, is(sameInstance(event)));
    }
}